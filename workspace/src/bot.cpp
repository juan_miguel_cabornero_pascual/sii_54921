#include"DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>

int main(){
	int fd;
	DatosMemCompartida *pdatos;
	fd=open("datos",O_RDWR);
	if (fd < 0) {
           perror("Error en la apertura del fichero de la proyeccion");
           exit(1);
        }
//mandato de proyeccion en memoria
	pdatos=static_cast<DatosMemCompartida*>(mmap(0, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0));
	if ((pdatos)==MAP_FAILED){
		perror("Error en la proyeccion del fichero");
		close(fd);
		return(1);
	}
	close(fd);
while(1){
	if(pdatos->esfera.centro.y<pdatos->raqueta1.getCentro().y){
		pdatos->accion=-1;
	}
	else if(pdatos->esfera.centro.y==pdatos->raqueta1.getCentro().y){
		pdatos->accion=0;
	}
	else if(pdatos->esfera.centro.y>pdatos->raqueta1.getCentro().y){
		pdatos->accion=1;
	}
	usleep(25000);
}
	munmap(pdatos,sizeof(DatosMemCompartida));
	unlink("datos");
return 1;
}
