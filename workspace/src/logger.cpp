#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "Marcador.h"

int main(){
	int fichero;
	Marcador puntos;
	int nfifo, nread;
//creacion de la tuberia
	nfifo=mkfifo("/tmp/FIFO",0600);
	if((nfifo)<0){
		perror("Error al crear el fifo");
		return 1;
	}
//abre la tuberia
	fichero=open("/tmp/FIFO",O_RDONLY);
	if((fichero)<0){
		perror("Error al leer el fifo");
		return 1;
	}
//lee la tuberia
	//nread=read(fichero,&puntos,sizeof(puntos));
	nread=read(fichero,&puntos,sizeof(puntos));
	if(nread<0){
		perror("Error en el numero de bytes leidos del fifo");
		return 1;
	}
	while(read(fichero,&puntos,sizeof(puntos))==sizeof(puntos)){ //cuando lo enviado por la tuberia coincida con la long de puntos
		if(puntos.ultganador==1)
			printf("Jugador 1 marca 1 punto, lleva un total de %d puntos.\n",puntos.jugador1);
		else if(puntos.ultganador==2)
			printf("Jugador 2 marca 1 punto, lleva un total de %d puntos.\n",puntos.jugador1);
		}

	close(fichero);
	unlink("/tmp/FIFO");//cierra la tuberia
	return(0);
}
